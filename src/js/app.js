var my_news = [
    {
        author: 'Саша Печкин',
        text: 'В четчерг, четвертого числа...',
        bigText: 'В четчерг, четвертого числа... Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium adipisci aliquam aliquid doloremque eaque facere harum, iste laborum libero, odit pariatur quibusdam repellat sequi sint temporibus veritatis vitae voluptas!'
    },
    {
        author: 'Просто Вася',
        text: 'Считаю, что $ должен стоить 35 рублей!',
        bigText: 'Считаю, что $ должен стоить 35 рублей! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda cumque delectus earum eligendi laborum nam necessitatibus nostrum odio placeat praesentium quis, saepe, sit sunt tempora temporibus. Ad quibusdam repudiandae vitae!'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        bigText: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad dolorem dolores doloribus earum id, itaque, laborum libero maiores nesciunt nobis non obcaecati pariatur perspiciatis quaerat quam ullam. At, fugiat.'
    }
];

window.ee = new EventEmitter();

var Article = React.createClass({
    propTypes: {
        data: React.PropTypes.shape({
            author: React.PropTypes.string.isRequired,
            text: React.PropTypes.string.isRequired,
            bigText: React.PropTypes.string.isRequired
        })
    },

    getInitialState: function() {
      return {
          visible: false,
          readmorebtn: 'показать основное'
      }
    },

    readmoreClick: function(e) {
      e.preventDefault();
        var textBtn = e.target.innerHTML;
        var newtextBtn = this.state.readmorebtn;
        var visible = this.state.visible ? false : true

        this.setState({
            visible: visible,
            readmorebtn: textBtn
        });

        e.target.innerHTML = newtextBtn;
    },

    render: function() {
        let author = this.props.data.author;
        let text = this.props.data.text;
        let bigText = this.props.data.bigText;
        let visible = this.state.visible;

        return (
            <div className="article">
                <p className="news__author">{author}</p>
                <p className="news__text">{text}</p>
                <a href="#"
                   className={'news__readmore '}
                   onClick={this.readmoreClick}>
                    Подробнее
                </a>
                <p className={'news__big-text ' + (visible ? '' : 'none')}>{bigText}</p>
            </div>
        );
    }
});

var News = React.createClass({
    propTypes: {
        data: React.PropTypes.array.isRequired
    },

    render: function() {
        let data = this.props.data;
        let newsTemplate;

        if ( data.length > 0 ) {
            newsTemplate = data.map(function(item, index) {
                return (
                    <div key={index}>
                        <Article data={item}/>
                    </div>
                );
            });
        } else {
            newsTemplate = <p>К сожалению новостей нет</p>
        }

        return (
            <div className="news">
                {newsTemplate}
                <strong className={'news__count ' + (data.length > 0 ? '' : 'none')}>Всего новостей: {data.length}</strong>
            </div>
        );
    }
});

var Add = React.createClass({
    componentDidMount: function() {
        ReactDOM.findDOMNode(this.refs.author).focus();
    },

    getInitialState: function() {
     return {
        authorIsEmpty: true,
        textIsEmpty: true,
        bigTextIsEmpty: true,
        agreeNotChecked : true
      }
    },

    onCheckRuleClick :function(e) {
        this.setState({
            agreeNotChecked: !e.target.checked
        });
    },

    onFieldChange: function(fieldName, e) {
        this.setState({
            [fieldName]: e.target.value.trim().length > 0 ? false : true
        });
    },

    sendNews: function(e) {
        e.preventDefault();
        let author = ReactDOM.findDOMNode(this.refs.author).value.trim();
        let text = ReactDOM.findDOMNode(this.refs.text).value.trim();
        let bigText = ReactDOM.findDOMNode(this.refs.bigText).value.trim()

        let item = [{
            author: author,
            text: text,
            bigText: bigText
        }];

        window.ee.emit('News.add', item);

        ReactDOM.findDOMNode(this.refs.text).value = '';
        ReactDOM.findDOMNode(this.refs.bigText).value = '';
        this.setState({textIsEmpty: true});
        this.setState({bigTextIsEmpty: true});
    },


    render: function() {
      return (
          <form action='' className="add cf">
              <input
                  type='text'
                  className='add__author'
                  placeholder='Ваше имя'
                  defaultValue=''
                  ref='author'
                  onChange={this.onFieldChange.bind(this, 'authorIsEmpty')}
              />

              <textarea
                  clasName="add__text"
                  placeholder='Краткое описание новости'
                  defaultValue=''
                  ref='text'
                  onChange={this.onFieldChange.bind(this, 'textIsEmpty')}
              ></textarea>

              <textarea
                  clasName="add__text"
                  placeholder='Текст новости'
                  defaultValue=''
                  ref='bigText'
                  onChange={this.onFieldChange.bind(this, 'bigTextIsEmpty')}
              ></textarea>

              <label className="add__checkrule">
                  <input
                      type="checkbox"
                      ref='checkrule'
                      onChange={this.onCheckRuleClick}
                  />
                  я согласен с правилами
              </label>

              <button
                  className='add__btn'
                  onClick={this.sendNews}
                  type='button'
                  ref="add_news"
                  disabled={ this.state.authorIsEmpty || this.state.textIsEmpty || this.state.bigTextIsEmpty || this.state.agreeNotChecked ? true : false}
              >
                  Add News
              </button>
          </form>
      )
    }
});

var App = React.createClass({
    getInitialState: function() {
      return {
          news: my_news
      }
    },

    componentDidMount: function() {
        let self = this;
        window.ee.addListener('News.add', function(item) {
            var nextNews = item.concat(self.state.news);
            self.setState({news: nextNews});
        });
    },

    componentWillUnmount: function() {
        window.ee.removeListener('News.add');
    },

    render: function() {
      return (
          <div className="app">
              <Add />
              <h3>Новости</h3>
              <News data={this.state.news}/>
          </div>
      );
    }
});

ReactDOM.render(
    <App />,
    document.getElementById('root')
);