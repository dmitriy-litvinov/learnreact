import gulp from 'gulp';

export default (config) => {

    // watch
    gulp.task('watch', function () {
        // gulp.watch([ config.paths.src.styles.libs ],  ['libsCss'] );
        gulp.watch([ config.paths.src.styles.appAll ],  ['styles'] );
        // gulp.watch([ config.paths.src.img ],  ['images'] );
        // gulp.watch([ config.paths.src.fonts ],  ['fonts'] );
        gulp.watch([ config.paths.src.scripts.libs ], ['libsJs']  );
        gulp.watch([ config.paths.src.scripts.coppies ], ['coppies']  );
        gulp.watch([ config.paths.src.scripts.app ], ['scripts']  );
        gulp.watch([ config.paths.src.templates  ],  ['html']   );
    });
    
    // default  
    gulp.task('default', [
        'connect',
        'html',
        'styles',
        // 'libsCss',
        'libsJs',
        'coppies',
        'scripts',
        // 'images',
        // 'fonts',
        'watch'
    ]);
    
};