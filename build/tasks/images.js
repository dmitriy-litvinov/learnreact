import gulp from 'gulp';

export default (config) => {
    gulp.task('images', function () {
        gulp.src(config.paths.src.img)
            .pipe(gulp.dest(config.paths.dist.img));
    });

    console.info('Gulp task *images* is loaded');
};