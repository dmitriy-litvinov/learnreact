import gulp from 'gulp';

export default (config) => {
    gulp.task('fonts', function () {
        gulp.src(config.paths.src.fonts)
            .pipe(gulp.dest(config.paths.dist.fonts));
    });

    console.info('Gulp task *fonts* is loaded');
};