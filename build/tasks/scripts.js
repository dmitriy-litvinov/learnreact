import gulp from 'gulp';
import notify from 'gulp-notify';
import connect from 'gulp-connect';
import concatJs  from'gulp-concat';
import babel  from'gulp-babel';
import uglifyJs  from'gulp-uglify';
import react  from'gulp-react';

export default (config) => {
    gulp.task('libsJs', function() {
        return gulp.src(config.paths.src.scripts.libs)
            .pipe(babel({presets: ['es2015'], compact: false}))
            .pipe(concatJs(config.paths.dist.scripts.libs.file))
            .pipe(uglifyJs())
            .pipe(gulp.dest(config.paths.dist.scripts.libs.dir))
            .pipe(notify('Done!'))
            .pipe(connect.reload());
    });
    
    gulp.task('scripts', function() {
        return gulp.src(config.paths.src.scripts.app)
            .pipe(react())
            .pipe(babel({presets: ['es2015']}))
            .pipe(concatJs(config.paths.dist.scripts.app.file))
            // .pipe(uglifyJs())
            .pipe(gulp.dest(config.paths.dist.scripts.app.dir))
            .pipe(notify('Done!'))
            .pipe(connect.reload());
    });

    gulp.task('coppies', function () {
        gulp.src(config.paths.src.scripts.coppies)
            .pipe(uglifyJs())
            .pipe(gulp.dest(config.paths.dist.scripts.coppies));
    });

    console.info('Gulp task *scripts* is loaded');
};