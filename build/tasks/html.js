import gulp from 'gulp';
import connect from 'gulp-connect';
// import dev from 'gulp-dev';

export default (config) => {
    gulp.task('html', function () {

        gulp.src(config.paths.src.templates)
            // .pipe(dev(true))
            .pipe(gulp.dest(config.paths.dist.templates.dir))
            .pipe(connect.reload());
    });

    console.info('Gulp task *html* is loaded');
};