import gulp from 'gulp';
import concatCss  from'gulp-concat-css';
import notify  from'gulp-notify';
import autoprefixer  from'gulp-autoprefixer';
import minifyCss  from'gulp-minify-css';
import sass  from'gulp-sass';
import sourcemaps  from'gulp-sourcemaps';
import connect from 'gulp-connect';

export default (config) => {

    gulp.task('styles', function () {
        return gulp.src(config.paths.src.styles.app)
            //.pipe(concatCss(config.paths.dist.styles.app.file))
            .pipe(sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer('last 100 versions', '> 1%', 'ie 9'))
            .pipe(minifyCss())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(config.paths.dist.styles.app.dir))
            .pipe(notify('Done!'))
            .pipe(connect.reload());
    });

    gulp.task('libsCss', function () {
        return gulp.src(config.paths.src.styles.libs)
            .pipe(concatCss(config.paths.dist.styles.libs.file))
            .pipe(autoprefixer('last 1 versions', '> 1%', 'ie 9'))
            .pipe(minifyCss())
            .pipe(gulp.dest(config.paths.dist.styles.libs.dir))
            .pipe(notify('Done!'))
            .pipe(connect.reload());
    });

    console.info('Gulp task *styles* is loaded');

};