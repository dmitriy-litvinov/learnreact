export default { // module.exports = {...}
    paths: {
        tasks: './build/tasks',
        src: {
            scripts:   {
                app: [
                    './src/js/**/*.{js,jsx}',
                ],
                libs: [
                    './node_modules/react/dist/react.js',
                    './node_modules/react-dom/dist/react-dom.js'
                ],
                coppies: './node_modules/wolfy87-eventemitter/EventEmitter.js'
            },
            styles:    {
                app: './src/scss/style.scss',
                appAll: './src/scss/**/*.scss',
                libs: [
                    './src/plugins/**/*.css',
                ]
            },
            templates: [
                './src/index.html'
            ],
            img: [
                './src/images/**/*.{jpg,png,gif,svg}'
            ],
            fonts: [
                './src/fonts/**/*.{eot,ttf,woff,woff2,svg,otf}'
            ]
        },
        dist: {
            serverRoot: './dist',
            scripts: {
                app: {
                    file: 'app.js',
                    dir: './dist/js'
                },
                libs: {
                    file: 'libs.js',
                    dir: './dist/js'
                },
                coppies: './dist/js'
            },
            styles: {
                app: {
                    file: 'style.css',
                    dir: './dist/css'
                },
                libs: {
                    file: 'libs.css',
                    dir: './dist/css'
                }
            },
            templates: {
                file: 'index.html',
                dir: './dist'
            },
            img: './dist/images',
            fonts: './dist/fonts'
        }
    }
};